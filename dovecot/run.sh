#!/bin/sh
DB_USER=$(aws ssm get-parameter --name $SSM_PARAM_USERNAME --region us-east-1 --with-decryption --output text --query Parameter.Value)
DB_PASSWORD=$(aws ssm get-parameter --name $SSM_PARAM_PASSWORD --region us-east-1 --with-decryption --output text --query Parameter.Value)

sed -i "2iconnect = host=$DB_HOST dbname=$DB_NAME user=$DB_USER password=$DB_PASSWORD" /etc/dovecot/dovecot-sql.conf
sed -i \
-e "s/^\(ssl_cert = <\/etc\/letsencrypt\/live\/\).*\(\/fullchain.pem\)/\1$CERT_FQDN\2/g" \
-e "s/^\(ssl_key = <\/etc\/letsencrypt\/live\/\).*\(\/privkey.pem\)/\1$CERT_FQDN\2/g" \
-e "s/^\(postmaster_address = postmaster\@\).*/\1$DOMAIN_NAME/g" \
/etc/dovecot/dovecot.conf

dovecot -F 
