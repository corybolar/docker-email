#!/bin/bash
cd "${0%/*}"
docker-compose stop postfix 2>&1
docker-compose kill postfix
docker-compose up -d postfix

docker-compose stop dovecot 2>&1
docker-compose kill dovecot
docker-compose up -d dovecot
