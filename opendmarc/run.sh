#!/bin/bash
set -m

rsyslogd 2> /dev/null &
sleep 2
sed -i "s/^AuthservID .*/AuthservID $DNS_HOSTNAME.$DOMAIN_NAME/g" /etc/opendmarc/opendmarc.conf
opendmarc -vvvv -c /etc/opendmarc/opendmarc.conf &
sleep 2
tail -f /var/log/mail.log &

pids=`jobs -p`

exitcode=0

function terminate() {
    trap "" CHLD

    for pid in $pids; do
        if ! kill -0 $pid 2>/dev/null; then
            wait $pid
            exitcode=$?
        fi
    done

    kill $pids 2>/dev/null
}

trap terminate CHLD
wait

exit $exitcode
