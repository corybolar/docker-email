#!/bin/bash
docker-compose kill opendmarc
docker-compose rm -f opendmarc
docker-compose build opendmarc
docker-compose up -d opendmarc
