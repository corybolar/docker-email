#!/bin/bash
cd "${0%/*}"
docker-compose stop nginx 2>&1
docker-compose kill nginx
docker-compose up -d nginx
