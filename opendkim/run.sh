#!/bin/bash
set -m

rsyslogd 2> /dev/null &
sleep 1
sed -i "s/example.com/$DOMAIN_NAME/g" /etc/opendkim/opendkim.conf;
echo "$DKIM_RECORD $DOMAIN_NAME:$SELECTOR:/etc/opendkim/keys/$DKIM_KEY" > /etc/opendkim/KeyTable;
echo "*@$DOMAIN_NAME $DKIM_RECORD" > /etc/opendkim/SigningTable; 
echo "$TRUSTED_HOSTS" > /etc/opendkim/TrustedHosts; 
sed -i 's/\s\+/\n/g' /etc/opendkim/TrustedHosts; 
chown -R opendkim:opendkim /etc/opendkim/*
aws ssm get-parameter --name $SSM_PARAM --region us-east-1 --with-decryption --output text --query Parameter.Value > /etc/opendkim/keys/mail.private
chown opendkim:opendkim /etc/opendkim/keys/mail.private
chmod 600 /etc/opendkim/keys/mail.private
opendkim &
sleep 1
tail -f /var/log/mail.log 

