<?php

/* Local configuration for Roundcube Webmail */

// ----------------------------------
// SQL DATABASE
// ----------------------------------
// Database connection string (DSN) for read+write operations
// Format (compatible with PEAR MDB2): db_provider://user:password@host/database
// Currently supported db_providers: mysql, pgsql, sqlite, mssql, sqlsrv, oracle
// For examples see http://pear.php.net/manual/en/package.database.mdb2.intro-dsn.php
// NOTE: for SQLite use absolute path (Linux): 'sqlite:////full/path/to/sqlite.db?mode=0646'
//       or (Windows): 'sqlite:///C:/full/path/to/sqlite.db'
$config['db_dsnw'] = 'mysql://roundcube:roundcube@mariadb-prod.cbpeckles.com/roundcube';

// log driver:  'syslog', 'stdout' or 'file'.
$config['log_driver'] = 'stdout';

// ----------------------------------
// IMAP
// ----------------------------------
// The IMAP host chosen to perform the log-in.
// Leave blank to show a textbox at login, give a list of hosts
// to display a pulldown menu or set one host as string.
// To use SSL/TLS connection, enter hostname with prefix ssl:// or tls://
// Supported replacement variables:
// %n - hostname ($_SERVER['SERVER_NAME'])
// %t - hostname without the first part
// %d - domain (http hostname $_SERVER['HTTP_HOST'] without the first part)
// %s - domain name after the '@' from e-mail address provided at login screen
// For example %n = mail.domain.tld, %t = domain.tld
// WARNING: After hostname change update of mail_host column in users table is
//          required to match old user data records with the new host.
$config['default_host'] = 'ssl://dovecot/';

// For STARTTLS IMAP
 $config['imap_conn_options'] = array(
     'ssl' => array(
       'verify_peer'       => false,
       'verify_peer_name'       => false,
       // certificate is not self-signed if cafile provided
       'allow_self_signed' => true,
       'cafile'  => '/etc/ssl/certs/Your_CA_certificate.pem',
       // For Letsencrypt use the following two lines and remove the 'cafile' option above.
       //'ssl_cert' => '/etc/letsencrypt/live/mail.my_domain.org/fullchain.pem',
       //'ssl_key'  => '/etc/letsencrypt/live/mail.my_domain.org/privkey.pem',
       // probably optional parameters
       'ciphers' => 'TLSv1+HIGH:!aNull:@STRENGTH',
       'peer_name'         => 'dovecot',
     ),
 );
 // For STARTTLS SMTP
 $config['smtp_conn_options'] = array(
     'ssl' => array(
       'verify_peer'       => false,
       'verify_peer_name'       => false,
       // certificate is not self-signed if cafile provided
       'allow_self_signed' => true,
       'cafile'  => '/etc/ssl/certs/Your_CA_certificate.pem',
       // For Letsencrypt use the following two lines and remove the 'cafile' option above.
       //'ssl_cert' => '/etc/letsencrypt/live/mail.my_domain.org/fullchain.pem',
       //'ssl_key'  => '/etc/letsencrypt/live/mail.my_domain.org/privkey.pem',
       // probably optional parameters
       'ciphers' => 'TLSv1+HIGH:!aNull:@STRENGTH',
       'peer_name'         => 'postfix',
     ),
 );
// TCP port used for IMAP connections
$config['default_port'] = 993;

$config['username_domain'] = 'cbpeckles.com';

$config['htmleditor'] = 4;

$config['use_https'] = true;

$config['login_autocomplete'] = 2;

#$config['session_domain'] = 'cbpeckles.com'

#$config['use_secure_urls'] = true;

// ----------------------------------
// SMTP
// ----------------------------------
// SMTP server host (for sending mails).
// Enter hostname with prefix tls:// to use STARTTLS, or use
// prefix ssl:// to use the deprecated SSL over SMTP (aka SMTPS)
// Supported replacement variables:
// %h - user's IMAP hostname
// %n - hostname ($_SERVER['SERVER_NAME'])
// %t - hostname without the first part
// %d - domain (http hostname $_SERVER['HTTP_HOST'] without the first part)
// %z - IMAP domain (IMAP hostname without the first part)
// For example %n = mail.domain.tld, %t = domain.tld
$config['smtp_server'] = 'tls://haproxy/';

// SMTP port (default is 25; use 587 for STARTTLS or 465 for the
// deprecated SSL over SMTP (aka SMTPS))
$config['smtp_port'] = 587;

// SMTP username (if required) if you use %u as the username Roundcube
// will use the current username for login
$config['smtp_user'] = '%u';

// SMTP password (if required) if you use %p as the password Roundcube
// will use the current user's password for login
$config['smtp_pass'] = '%p';

// provide an URL where a user can get support for this Roundcube installation
// PLEASE DO NOT LINK TO THE ROUNDCUBE.NET WEBSITE HERE!
$config['support_url'] = 'https://www.cbpeckles.com';

// This key is used for encrypting purposes, like storing of imap password
// in the session. For historical reasons it's called DES_key, but it's used
// with any configured cipher_method (see below).
$config['des_key'] = 'aTr3cIrGetSLEQVzQGLBnnHQ';

// Name your service. This is displayed on the login screen and in the window title
$config['product_name'] = 'CBPeckles Roundcube';

// ----------------------------------
// PLUGINS
// ----------------------------------
// List of active plugins (in plugins/ directory)
//$config['plugins'] = array();

// the default locale setting (leave empty for auto-detection)
// RFC1766 formatted language name like en_US, de_DE, de_CH, fr_FR, pt_BR
$config['language'] = 'en_US';

// Set the spell checking engine. Possible values:
// - 'googie'  - the default (also used for connecting to Nox Spell Server, see 'spellcheck_uri' setting)
// - 'pspell'  - requires the PHP Pspell module and aspell installed
// - 'enchant' - requires the PHP Enchant module
// - 'atd'     - install your own After the Deadline server or check with the people at http://www.afterthedeadline.com before using their API
// Since Google shut down their public spell checking service, the default settings
// connect to http://spell.roundcube.net which is a hosted service provided by Roundcube.
// You can connect to any other googie-compliant service by setting 'spellcheck_uri' accordingly.
$config['spellcheck_engine'] = 'pspell';
$config['skin'] = 'elastic';
