#!/bin/sh

sa-update;

if [ -n $LOGLEVEL ]; then
sed -i "s/^\$log_level = .*/\$log_level = $LOGLEVEL;/g" /etc/amavisd.conf;
fi

amavisd -c /etc/amavisd.conf foreground
