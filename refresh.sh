#!/bin/bash 

set -e

cd "${0%/*}"
echo $(pwd)

docker-compose build $1
docker-compose stop $1
docker-compose rm -f $1
docker-compose up -d $1
