#!/bin/bash
DB_USER=$(aws ssm get-parameter --name $SSM_PARAM_USERNAME --region us-east-1 --with-decryption --output text --query Parameter.Value)
DB_PASSWORD=$(aws ssm get-parameter --name $SSM_PARAM_PASSWORD --region us-east-1 --with-decryption --output text --query Parameter.Value)
for i in virtual_alias_maps.cf virtual_mailbox_maps.cf virtual_mailbox_domains.cf
do
	sed -i -e "s/^\(user =\).*/\1 $DB_USER/g" \
	-e "s/^\(password =\).*/\1 $DB_PASSWORD/g" \
	-e "s/^\(hosts =\).*/\1 $DB_HOST/g" \
	-e "s/^\(dbname =\).*/\1 $DB_NAME/g" /etc/postfix/$i
done

sed -i -E \
-e "1,/^(#|)myhostname =.*$/{s/^(#|)myhostname =.*$/myhostname = $HOSTNAME.$DOMAIN_NAME/}" \
-e "s/^(#|)mydomain =.*/mydomain = $DOMAIN_NAME/" \
/etc/postfix/main.cf

sed -i \
-e "s/^\(smtpd_tls_cert_file = \/etc\/letsencrypt\/live\/\).*\(\/fullchain.pem\)/\1$CERT_FQDN\2/" \
-e "s/^\(smtpd_tls_key_file = \/etc\/letsencrypt\/live\/\).*\(\/privkey.pem\)/\1$CERT_FQDN\2/" \
-e "s/^\(smtp_tls_CAfile = \/etc\/letsencrypt\/live\/\).*\(\/chain.pem\)/\1$CERT_FQDN\2/" \
/etc/postfix/main.cf

/usr/sbin/postfix start-fg &

trap "postfix stop; exit 0" SIGINT
trap "postfix stop; exit 0" SIGTERM
trap "postfix reload; exit 0" SIGHUP

sleep 3

while kill -0 "`cat /var/spool/postfix/pid/master.pid`"; do
  sleep 1
done
# MAIN.CF
# MASTER.CF
# HEADER_CHECKS
# MEMCACHED
#    sed -i "s/^\(smtpd_tls_cert_file = \/etc\/letsencrypt\/live\/\).*\(\/fullchain.pem\)/\1$CERT_FQDN\2/" /etc/postfix/main.cf; \
#    sed -i "s/^\(smtpd_tls_key_file = \/etc\/letsencrypt\/live\/\).*\(\/privkey.pem\)/\1$CERT_FQDN\2/" /etc/postfix/main.cf; \
#    sed -i "s/^\(smtp_tls_CAfile = \/etc\/letsencrypt\/live\/\).*\(\/chain.pem\)/\1$CERT_FQDN\2/" /etc/postfix/main.cf; \
