FROM alpine:edge

#ARG DOMAIN_NAME
#ARG HOSTNAME
#ARG CERT_FQDN

RUN apk add -U --no-cache postfix-mysql postfix postfix-pcre bash tini python curl; \
    cp /etc/postfix/main.cf /etc/postfix/main.cf.default; \
    cp /etc/postfix/master.cf /etc/postfix/master.cf.default; \
    curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"; \
    unzip awscli-bundle.zip; \
    ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws;
# MAIN.CF
COPY main.cf.additions /etc/postfix/main.cf.additions
RUN sed -E -i \
    -e "s/^(#|)compatibility_level =.*/compatibility_level = 2/g" \
#    -e "1,/^(#|)myhostname =.*$/{s/^(#|)myhostname =.*$/myhostname = $HOSTNAME.$DOMAIN_NAME/}" \
#    -e "s/^(#|)mydomain =.*/mydomain = $DOMAIN_NAME/" \
    -e "1,/^(#|)inet_interfaces =.*$/{s/^(#|)inet_interfaces =.*$/inet_interfaces = all/}" \
    -e "1,/^(#|)myorigin =.*$/{s/^(#|)myorigin =.*$/myorigin = \$mydomain/}" \
    -e "1,/^(#|)mydestination =.*$/{s/^(#|)mydestination =.*$/mydestination = \$myhostname, localhost.\$mydomain, localhost/}" \
    -e "1,/^(#|)local_recipient_maps =.*$/{s/^(#|)local_recipient_maps =.*$/local_recipient_maps = \$virtual_mailbox_maps/}" \
    -e "s/^(#|)unknown_local_recipient_reject_code =.*/unknown_local_recipient_reject_code = 450/" \
    -e "1,/^(#|)mynetworks =.*$/{s/^(#|)mynetworks =.*$/mynetworks = 10.0.0.0\/8, 127.0.0.0\/8, 172.16.0.0\/12/}" \
    -e "s/^(#|)relay_domains =.*/relay_domains = \$mydestination/" \
    -e "1,/^(#|)alias_maps =.*$/{s/^(#|)alias_maps =.*$/alias_maps = hash:\/etc\/aliases/}" \
    -e "1,/^(#|)alias_database =.*$/{s/^(#|)alias_database =.*$/alias_database = hash:\/etc\/aliases/}" \
    -e "s/^(#|)header_checks =.*/header_checks = regexp:\/etc\/postfix\/header_checks\nsmtp_header_checks = regexp:\/etc\/postfix\/header_checks\nmilter_header_checks = regexp:\/etc\/postfix\/header_checks/" \
    -e "1,/^(#|)smtpd_banner =.*$/{s/^(#|)smtpd_banner =.*$/smtpd_banner = \$myhostname ESMTP \$mail_name/}" \
    -e "s/^(#|)inet_protocols =.*$/inet_protocols = ipv4/" \
    /etc/postfix/main.cf; \
    cat /etc/postfix/main.cf.additions >> /etc/postfix/main.cf; \
    rm /etc/postfix/main.cf.additions;
# MASTER.CF
COPY master.cf.additions /etc/postfix/master.cf.additions
COPY submission_header_checks /etc/postfix/submission_header_checks
RUN sed -E -i \
    #-e "/^smtp      inet  n       -       n       -       -       smtpd$/a \  -o content_filter=amavisd:[amavisd]:10024" \
    # Postscreen
    -e "s/^(smtp\s*inet\s*n\s*-\s*n\s*-\s*-\s*smtpd)$/\#\1/" \
    -e "s/^\#(smtp\s*inet\s*n\s*-\s*n\s*-\s*1\s*postscreen)$/\1/" \
    -e "s/^\#(smtpd\s*pass\s*-\s*-\s*n\s*-\s*-\s*smtpd)$/\1/" \
    -e "/^smtpd\s*pass\s*-\s*-\s*n\s*-\s*-\s*smtpd$/a \  -o content_filter=amavisd:[amavisd]:10024" \
    -e "s/^\#(dnsblog\s*unix\s*-\s*-\s*n\s*-\s*0\s*dnsblog)$/\1/" \
    -e "s/^\#(tlsproxy\s*unix\s*-\s*-\s*n\s*-\s*0\s*tlsproxy)$/\1/" \
    # Submission
    -e "s/^\#(submission.*)/\1/" \
    -e "/^submission.*/a \  -o smtpd_upstream_proxy_protocol=haproxy" \
    -e "/^submission.*/a \  -o smtpd_sasl_tls_security_options=noanonymous" \
    -e "/^submission.*/a \  -o smtpd_sasl_security_options=noanonymous,noplaintext" \
    -e "/^submission.*/a \  -o smtpd_client_restrictions=permit_sasl_authenticated,reject_unauth_destination,reject" \
    -e "/^submission.*/a \  -o smtpd_sasl_auth_enable=yes" \
    -e "/^submission.*/a \  -o smtpd_enforce_tls=yes"\
    -e "/^submission.*/a \  -o smtpd_tls_security_level=encrypt" \
    # Submission port filtering
    -e "/^submission.*/a \  -o cleanup_service_name=subcleanup" \
    -e "/^cleanup.*/a subcleanup unix n	-	-	-	0	cleanup" \
    /etc/postfix/master.cf; \
    sed -E -i -e "/^subcleanup.*/a \  -o header_checks=regexp:/etc/postfix/submission_header_checks" /etc/postfix/master.cf; \
    chmod 644 /etc/postfix/submission_header_checks; \
    cat /etc/postfix/master.cf.additions >> /etc/postfix/master.cf; \
    touch /etc/postfix/postscreen_access.cidr; \
    rm /etc/postfix/master.cf.additions;
COPY postscreen_dnsbl_reply_map.pcre /etc/postfix/postscreen_dnsbl_reply_map.pcre
# HEADER_CHECKS
RUN echo "/^X-Originating-IP:/                    	IGNORE" >> /etc/postfix/header_checks; \
    echo "/^X-Mailer:/                            	IGNORE" >> /etc/postfix/header_checks; \
    echo "/^Received: from localhost \(email_amavisd/	IGNORE" >> /etc/postfix/header_checks; \
    # Log subject lines in maillog
    echo "/^Subject:/     WARN" >> /etc/postfix/header_checks; \
# MEMCACHED
    echo "memcache = inet:memcached:11211" >> /etc/postfix/postscreen_cache; \
    echo "key_format = postscreen:%s" >> /etc/postfix/postscreen_cache; \
    chmod 600 /etc/postfix/postscreen_dnsbl_reply_map.pcre;
        
COPY virtual_alias_maps.cf /etc/postfix/virtual_alias_maps.cf
COPY virtual_mailbox_maps.cf /etc/postfix/virtual_mailbox_maps.cf
COPY virtual_mailbox_domains.cf /etc/postfix/virtual_mailbox_domains.cf
COPY hold /etc/postfix/hold
COPY run.sh /run.sh
RUN chmod 644 /etc/postfix/header_checks; \
    chmod 644 /etc/postfix/virtual_alias_maps.cf; \
    chmod 644 /etc/postfix/virtual_mailbox_maps.cf; \
    chmod 644 /etc/postfix/virtual_mailbox_domains.cf; \
    chmod 644 /etc/postfix/hold; \
#    sed -i "s/^\(smtpd_tls_cert_file = \/etc\/letsencrypt\/live\/\).*\(\/fullchain.pem\)/\1$CERT_FQDN\2/" /etc/postfix/main.cf; \
#    sed -i "s/^\(smtpd_tls_key_file = \/etc\/letsencrypt\/live\/\).*\(\/privkey.pem\)/\1$CERT_FQDN\2/" /etc/postfix/main.cf; \
#    sed -i "s/^\(smtp_tls_CAfile = \/etc\/letsencrypt\/live\/\).*\(\/chain.pem\)/\1$CERT_FQDN\2/" /etc/postfix/main.cf; \
    adduser -u 5000 -s /usr/bin/nologin -h /home/vmail -D vmail2; \
    postmap hash:/etc/postfix/hold; \
    chmod 755 /run.sh

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/run.sh"]

HEALTHCHECK --interval=30s --timeout=5s \
  CMD postfix status
